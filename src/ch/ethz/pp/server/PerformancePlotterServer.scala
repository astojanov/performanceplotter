/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

package ch.ethz.pp.server

import org.eclipse.jetty.server._
import org.eclipse.jetty.util.ssl.SslContextFactory
import com.typesafe.config.ConfigFactory
import org.eclipse.jetty.servlet.{ServletContextHandler, ServletHolder}
import org.eclipse.jetty.server.handler.{HandlerList, DefaultHandler, ResourceHandler, ContextHandlerCollection}

class PerformancePlotterServer extends Server {

  private val config  = ConfigFactory.load
  private val port    = config.getInt("pp.port")

  private val rootPath            = "./www/"
  private val keystorePath        = getClass.getResource("/ppkeystore").toExternalForm()
  private val keyStorePassword    = config.getString("pp.keyStorePassword")
  private val keyManagerPassword  = config.getString("pp.keyManagerPassword")

  def init () = {

    // Default HTTPS configuration
    val https = new HttpConfiguration()
    https.addCustomizer(new SecureRequestCustomizer())

    // Create SSL Connection using ./conf/ppkeystore as keystore
    val sslContextFactory = new SslContextFactory()
    sslContextFactory.setKeyStorePath(keystorePath)
    sslContextFactory.setKeyStorePassword(keyStorePassword)
    sslContextFactory.setKeyManagerPassword(keyManagerPassword)

    // Generate the connector
    val sslConnector = new ServerConnector(this,
      new SslConnectionFactory(sslContextFactory, "http/1.1"),
      new HttpConnectionFactory(https))
    sslConnector.setPort(port)
    setConnectors(List(sslConnector).toArray)

    //    config.setOutputBufferSize(32786)
    //    config.setRequestHeaderSize(8192)
    //    config.setResponseHeaderSize(8192)

    val plotterContext = new ServletContextHandler()
    plotterContext.setContextPath("/plotter/")
    plotterContext.addServlet(new ServletHolder(new ResultsServlet()),"/results/")
    plotterContext.addServlet(new ServletHolder(new ResultsPServlet()),"/resultsp/")
    plotterContext.addServlet(new ServletHolder(new SubmitServlet()),"/submit/")
    plotterContext.addServlet(new ServletHolder(new AuthenticationServler()),"/login/")

    // Create the ResourceHandler. It is the object that will actually handle the request for a given file. It is
    // a Jetty Handler object so it is suitable for chaining with other handlers.
    val resurceHandler = new ResourceHandler()
    resurceHandler.setDirectoriesListed(true)
    resurceHandler.setWelcomeFiles(Array("index.html"))
    resurceHandler.setResourceBase(rootPath)

    val handlers = new HandlerList();
    handlers.setHandlers(Array(resurceHandler, plotterContext))

    setHandler(handlers)
    start()
  }
}