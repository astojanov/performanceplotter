/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function AdminUI (rootElement) {

    this.name = "admin";
    this._hash = {};
    this._menuBar = null;
    this._sessions = [];

    this._html = " \
        <div id=\"menubar\"></div> \
        <div id=\"sidebar\"><div id=\"sessions\"><ul>\
            <li>\
                <h3>Sessions</h3>\
                <p>Edit / Delete Sessions</p>\
            </li>\
            <li>\
                <h3>Settings</h3>\
                <p>User Access Rights, Passwords</p>\
             </li>\
        </ul></div></div>\
        <div id='mainContainer'> \
            <div class='outer'><div class='middle'> \
                <div id='adminSessionsContainer'></div>\
            </div></div> \
        </div>";


    this._hashInit = function () {
        var hash = $.deserialize(window.location.hash);
        this._hash['section'] = (hash['section']) ? hash['section'] : 'sessions';
        this._hash['m'] = "admin";
        $.updateHash(this._hash);
    }

    this._loginScreen = function (successF, failureF) {
        var html = "\
            <div class='outer'><div class='middle'> \
                <div id='loginForm'><table style='border-collapse: collapse; text-align: center; width: 100%; height: 100%;    '> \
                    <tr>\
                        <td><input id='txtUsername' type='text' name='username' value='Username'></td> \
                    </tr><tr>\
                        <td><input id='txtPassword' type='password' name='password' value='Password'></td> \
                    </tr><tr>\
                        <td><div id='loginButton'>Login</div></td>\
                    </tr>\
                </table></div> \
            </div></div>"
        $(html).appendTo(rootElement);

        $('#txtUsername').focus ( function() { if ($(this).val() === "Username") { $(this).val(""); }});
        $('#txtUsername').blur  ( function() { if ($(this).val() === "") { $(this).val("Username"); }});
        $('#txtPassword').focus ( function() { if ($(this).val() === "Password") { $(this).val(""); }});
        $('#txtPassword').blur  ( function() { if ($(this).val() === "") { $(this).val("Password"); }});

        var cleanUp = function () {
            $('#txtUsername').off('focus');
            $('#txtUsername').off('blur');
            $('#txtPassword').off('focus');
            $('#txtPassword').off('blur');
            $('#loginButton').off('click');
            rootElement.empty();
        }

        $('#loginButton').click (function() {
            var username = $('#txtUsername').val();
            var password = $('#txtPassword').val();
            if (mainCommunicator.login(username, password)) {
                cleanUp ();
                successF ();
            } else {
                cleanUp ();
                failureF ();
            }
        })
    }

    this._login = function ( successF, failureF ) {
        if (mainCommunicator.isAuthenticated()) {
            successF ();
        } else {
            this._loginScreen(successF, failureF);
        }
    }

    this.init = function () {
        var that = this;
        var loginSuccess = function () {
            $(that._html).appendTo(rootElement);
            that._menuBar = new MenuBar($('#menubar'))
            that._menuBar.init();
            that._hashInit();
            that._initSize ();
            that.getAllSessions ();
        }
        var loginFailure = function () {

        }
        this._login (loginSuccess, loginFailure);
    }


    this._initSize = function  () {
        $('#mainContainer').css({ "position" : "absolute" });
        $('#sidebar').css({ "position" : "fixed" });
        var onWindowResize = function () {
            var w = $('body').width() - $("#sidebar").width();
            var h = $('body').height() - $('#menubar').outerHeight();
            $('#mainContainer').css({
                "width"  : w + "px",
                "height" : h + "px"
            });
            $('#sidebar').css({
                "margin-top" : $('#menubar').outerHeight() + "px",
                "min-height" : h + "px"
            });
        }
        $(window).resize(onWindowResize)
        onWindowResize ();
    }


    this.getAllSessions = function () {

        var that = this;

        mainCommunicator.getSession("all", function (data) {

            var currentUsersSessions = [];
            var usersSessionsMap = {};

            $.each(data['sessions'], function (idx, session) {
                if (session['uid'] === mainCommunicator.uid) {
                    currentUsersSessions.push(session);
                } else {
                    var sessionArray = null;
                    if (session['uid'] in usersSessionsMap) {
                        sessionArray = usersSessionsMap[session['uid']];
                    } else {
                        sessionArray = [];
                    }
                    sessionArray.push(session);
                    usersSessionsMap[session['uid']] = sessionArray;
                }
            });

            var createSessions = function(idx, session, div) {
                var item = $('<li/>');
                item.appendTo(div);
                if ( idx % 2 == 1 ) item.css({
                    "background-color": "#f3f3f3"
                });
                var s = new AdminSessionUI(item, session);
                s.init ();
                that._sessions.push(s);
            }

            if ( currentUsersSessions.length > 0 ) {
                $("<h2>" + currentUsersSessions[0]['userFullName'] + "</h2>")
                    .appendTo($('#adminSessionsContainer'));
                var usersList = $('<ul/>');
                usersList.appendTo($('#adminSessionsContainer'));
                $.each(currentUsersSessions, function(key, value) {
                    createSessions(key, value, usersList)
                });
            }

            $.each(usersSessionsMap, function (uid, sessionArray){
                $("<h2>" + sessionArray[0]['userFullName'] + "</h2>")
                    .appendTo($('#adminSessionsContainer'));
                var otherList = $('<ul/>');
                otherList.appendTo($('#adminSessionsContainer'));
                $.each(sessionArray, function(key, value) {
                    createSessions(key, value, otherList)
                });
            });

        });
    }





    this.hashChange = function () {

    }

    this.clean = function () {
        if (this._menuBar !== null)
            this._menuBar.clean();
        rootElement.empty();
    }
}