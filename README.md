
### INSTALL:

1. Make sure that you have `openssl` and `JVM` (including `keytool`) installed on your system

2. Run `script/generateSSL.sh` which is required to generate SSL certificates for HTTPS connection. If you want to customize this process, follow the explanation in the second section.

3. Make sure that the passwords created in the `script/generateSSL.sh` script match the one in `conf/application.conf`

4. Configure the port in `conf/application.conf`

### SSL Certificates for JVM keystore

Assuming that you want to use the alias "domain.com" to store the key and certificate in the keystore, you can use the following commands to get the job done:

`keytool -keystore keystore.jks -import -alias root -file z.crt -trustcacerts`

which will import your root certificate (or the chain file). Then you can import your certificate:

`keytool -keystore keystore.jks -import -alias domain.com -file x -trustcacerts`

Finally, you use openssl tool to convert the private key into pkcs12, and import it into the keystore.

`openssl pkcs12 -export -in x -inkey y -out domain.pkcs12`

`keytool -importkeystore -srckeystore domain.pkcs12 -srcstoretype PKCS12 -destkeystore domain.com`


### HOW TO RUN:

1. Install the simple build tool ([SBT](http://www.scala-sbt.org/)). 
You will need version 0.12.0 of [sbt-launch.jar](http://repo.typesafe.com/typesafe/ivy-releases/org.scala-sbt/sbt-launch/0.12.0/sbt-launch.jar). 
Follow the [installation instructions](http://www.scala-sbt.org/download.html#manual) on the SBT website.

2. Run `sbt run` to start the server.


