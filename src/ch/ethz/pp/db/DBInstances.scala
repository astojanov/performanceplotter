/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

package ch.ethz.pp.db

import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import java.sql.Timestamp

trait DBInstances {

  case class Session(id: Int, commitID: String, title: String, timestamp: Timestamp, uid: Int, maxPerf: Double, os: String,
                     compiler: String, flags: String, cpuModel: String, cpuFreq: Long, ramSize: Long, ramFreq: Long, opt: String) {
    def toJSON () = {
      ("id" -> id) ~ ("title" -> title) ~ ("commitID" -> commitID) ~ ("timestamp" -> timestamp.getTime) ~ ("uid" -> uid) ~
        ("maxPerf" -> maxPerf) ~ ("os" -> os) ~ ("compiler" -> compiler) ~ ("flags" -> flags) ~ ("cpuModel" -> cpuModel) ~
        ("cpuFreq" -> cpuFreq) ~ ("ramSize" -> ramSize) ~ ("ramFreq" -> ramFreq) ~ ("opt" -> opt)
    }
  }

  case class User(id: Int, username: String, password: String, name: String) {
    def toJSON() = {
      ("id" -> id) ~ ("username" -> username) ~ ("password" -> password) ~ ("name" -> name)
    }
  }

  case class DataInstance(id: Int, sid: Int, xaxis: Double, flops: Double, cycles: Double, series: String, opt: String) {
    def toJSON() = {
      ("id" -> id) ~ ("sid" -> sid) ~ ("xaxis" -> xaxis) ~ ("flops" -> flops) ~
        ("cycles" -> cycles) ~ ("series" -> series) ~ ("opt" -> opt)
    }
  }


}
