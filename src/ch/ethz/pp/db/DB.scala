/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

package ch.ethz.pp.db

import scala.slick.driver.SQLiteDriver.simple._
import scala.slick.jdbc.meta.MTable
import java.sql.Timestamp
import scala.slick.jdbc.StaticQuery

object DB extends DBInstances {

  // Make database connection
  val database = Database.forURL("jdbc:sqlite:plots.db", driver = "org.sqlite.JDBC")

  class Users(tag: Tag) extends Table[User](tag, "users") {
    def id        = column[Int]  ("id", O.PrimaryKey, O.AutoInc) // This is the primary key column
    def username  = column[String]("username")
    def password  = column[String]("password")
    def name      = column[String]("name")
    def * = (id, username, password, name) <> (User.tupled, User.unapply)
  }
  val users = TableQuery[Users]

  class Sessions(tag: Tag) extends Table[Session](tag, "sessions") {
    def id        = column[Int]  ("id", O.PrimaryKey, O.AutoInc) // This is the primary key column
    def commitID  = column[String]("commitID")
    def title     = column[String]("title")
    def timestamp = column[Timestamp]("timestamp")
    def uid       = column[Int]  ("uid")
    def maxPerf   = column[Double]("maxPerf")
    def os        = column[String]("os")
    def compiler  = column[String]("compiler")
    def flags     = column[String]("flags")
    def cpuModel  = column[String]("cpuModel")
    def cpuFreq   = column[Long]  ("cpuFreq")
    def ramSize   = column[Long]  ("ramSize")
    def ramFreq   = column[Long]  ("ramFreq")
    def opt       = column[String]("opt")
    def * = (id, commitID, title, timestamp, uid, maxPerf, os, compiler, flags, cpuModel, cpuFreq, ramSize, ramFreq, opt) <> (Session.tupled, Session.unapply)
    def userID = foreignKey("uid_fk", id, users)(_.id)
  }
  val sessions = TableQuery[Sessions]

  class Data(tag: Tag) extends Table[DataInstance](tag, "data") {
    def id     = column[Int]   ("id", O.PrimaryKey, O.AutoInc)
    def sid    = column[Int]   ("sid")
    def xaxis  = column[Double]("xaxis")
    def flops  = column[Double]("flops")
    def cycles = column[Double]("cycles")
    def series = column[String]("series")
    def opt    = column[String]("opt")
    def * = (id, sid, xaxis, flops, cycles, series, opt) <> (DataInstance.tupled, DataInstance.unapply)
    def sessionID = foreignKey("sid_fk", id, sessions)(_.id)
  }
  val data = TableQuery[Data]

  def initialize () = database withSession { implicit session =>
    if (MTable.getTables("users").list().isEmpty) (users.ddl).create
    if (MTable.getTables("sessions").list().isEmpty) (sessions.ddl).create
    if (MTable.getTables("data").list().isEmpty) (data.ddl).create
  }

  def getAllSessions () : List[Session] = database withSession { implicit session =>
    sessions.sortBy(_.timestamp.desc).list
  }

  def insertSession(s: Session) = this.synchronized {
    database withSession { implicit session =>
      sessions.insert(s)
      sessions.sortBy(_.id.desc).list.head
    }
  }

  def deleteSession(s: Session) = this.synchronized {
    database withSession { implicit session =>
      data.filter(_.sid === s.id).delete
      sessions.filter(_.id === s.id).delete
    }
  }

  def updateSession(s: Session) = this.synchronized {
    database withSession { implicit session =>
      sessions.filter(_.id === s.id).update(s)
    }
  }

  def insertDataInstance(d: DataInstance) = this.synchronized {
    database withSession { implicit session =>
      data.insert(d)
    }
  }

  def deleteDataInstance(d: DataInstance) = this.synchronized {
    database withSession { implicit session =>
      data.filter(_.id === d.id).delete
    }
  }

  def getLastSession () : Option[Session] = database withSession { implicit session =>
    if (!sessions.list().isEmpty) Some(sessions.sortBy(_.timestamp.desc).list.head) else None
  }

  def getSession(id: Int) : Option[Session] = database withSession { implicit session =>
    val q = sessions.filter(_.id === id).list
    if (!q.isEmpty) Some(q.head) else None
  }

  def getUserByID(id: Int) : Option[User] = database withSession { implicit session =>
    val q = users.filter(_.id === id).list
    if (!q.isEmpty) Some(q.head) else None
  }

  def getUserByUsername(username: String) : Option[User] = database withSession { implicit session =>
    val q = users.filter(_.username === username).list
    if (!q.isEmpty) Some(q.head) else None
  }

  def getDataInstancesBySID(sid: Int) : List[DataInstance] = database withSession { implicit session =>
    data.filter(_.sid === sid).list
  }
}
