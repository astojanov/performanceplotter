/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function PlotterBase () {

    this._mode = "performance";

    this.setMode = function (mode) {
        this._mode = mode;
    }

    this._createSubtitle = function (session, delim) {
        var arch = delim + "CPU: " + session['cpuModel'] + " " + session['cpuFreq'] + "MHz, ";
        arch += "RAM: " + session['ramSize'] + "MB " + session['ramFreq'] + "MHz, ";
        arch += "OS: " + session['os'] + "<br />"
        arch += delim + "CC: " + session['compiler'] + " " + session['flags'];
        return arch;
    }

    this._renamingFunctions = function () {
        var functions = [];
        functions.push ( function(s, serie) { return serie; });
        functions.push ( function(s, serie) { return serie + " " + s.getField('title'); });
        functions.push ( function(s, serie) { return serie + " " + s.getField('title') + " (" + s.getID() + ")"; });
        return functions;
    }

    this._addDataInstance = function (serie, dataInstance) {
        var yValue = 0;
        if ( this._mode === "performance" ) {
            yValue = dataInstance['flops'] / dataInstance['cycles'];
        } else if (this._mode === "cycles") {
            yValue = dataInstance['cycles'];
        }
        serie["data"].push({
            x : dataInstance['xaxis'],
            y : yValue,
            opt : dataInstance['opt']
        });
    }

    this._mergeTitles = function(sessions) {
        var title = sessions[0].getField('title');
        for (var i = 1; i < sessions.length; i++) {
            title = title + " / " + sessions[i].getField('title');
        }
        return title;
    }

    this._mergeSubtitles = function(sessions) {
        var sameSubTitle = true;
        var session = sessions[0].getSession();
        var subtitle = this._createSubtitle(session, "");
        for (var i = 1; i < sessions.length; i++) {
            var session = sessions[i].getSession();
            if (subtitle !== this._createSubtitle(session, "")) {
                sameSubTitle = false;
                break;
            }
        }
        if (!sameSubTitle) {
            var session = sessions[0].getSession();
            var tab = " - ";
            subtitle = session['title'] + "<br />" + this._createSubtitle(session, tab);
            for ( var i = 1; i < sessions.length; i++) {
                session = sessions[i].getSession ();
                subtitle += "<br />" + session['title']  + "<br/>" + this._createSubtitle(session, tab);
            }
            subtitle += "<br/>";
        }
        return subtitle;
    }

    this._mergeMaxPerf = function (sessions) {
        var maxPerf = sessions[0].getField('maxPerf');
        for (var i = 1; i < sessions.length; i++) {
            if (maxPerf < sessions[i].getField('maxPerf')) {
                maxPerf = sessions[i].getField('maxPerf')
            }
        }
        return maxPerf;
    }

    this._mergeSIDs = function(sessions) {
        var sid = "" + sessions[0].getID();
        for (var i = 1; i < sessions.length; i++) {
            sid = sid + "_" + sessions[i].getID();
        }
        return sid;
    }

    this._mergeSessions = function (sessions) {
        var that = this;
        return {
            sids     : that._mergeSIDs(sessions),
            title    : that._mergeTitles(sessions),
            subtitle : that._mergeSubtitles(sessions),
            maxPerf  : that._mergeMaxPerf(sessions)
        }
    }

    this._mergeSeries = function (sessions) {

        var series = [];
        var map = {};
        var that = this;
        var f = that._findBestRenamingFit(sessions);

        $.each(sessions, function(idx, session){
            $.each(session.getData(), function(id, dataInstance){
                var idx = 0;
                var sName = f(session, dataInstance['series']);
                if (sName in map) {
                    idx = map[sName];
                } else {
                    series.push({
                        "name" : sName,
                        "data" : new Array ()
                    })
                    idx = series.length - 1;
                    map[sName] = idx;
                }
                that._addDataInstance(series[idx], dataInstance);
            })
        });
        var sortByValue = function (a, b) {
            if (a.x < b.x) return -1; else if (a.x == b.x) return 0; else return 1;
        }
        var sortByName = function (a, b) {
            if (a.name < b.name) return -1; else if (a.name == b.name) return 0; else return 1;
        }
        $.each(series, function(id, serie) { serie["data"].sort(sortByValue); })
        return series.sort(sortByName);
    }

    this._findBestRenamingFit = function (sessions) {
        var strategies = this._renamingFunctions ();
        var bestFit = null;
        if (sessions.length === 1) {
            bestFit = strategies[0];
        } else {
            for (var idx = 0; idx < strategies.length; idx++) {
                var f = strategies[idx];
                var conflict = false;
                for(var i = 0; i < sessions.length && !conflict; i++) {
                    for (var j = i+1; j < sessions.length && !conflict; j++) {
                        $.each(sessions[i].getSeries(), function(i1, s1){
                            $.each(sessions[j].getSeries(), function(i2, s2){
                                if (f(sessions[i], s1) === f(sessions[j], s2)) {
                                    conflict = true;
                                }
                            });
                        })
                    }
                }
                if (!conflict) {
                    bestFit = f;
                    break;
                }
            }

        }
        return bestFit;
    }

}