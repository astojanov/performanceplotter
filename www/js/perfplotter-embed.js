/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

var _perfplotter_css  = [];
var _perfplotter_includes  = [];
var _perfplotter_instances = {};
var _perfplotter_filename  = "js/perfplotter-embed.js";

_perfplotter_css.push("css/fonts.css");

_perfplotter_includes.push("js/lib/json2.js");
_perfplotter_includes.push("js/lib/highcharts.js");
_perfplotter_includes.push("js/lib/jquery.deserialize.js");
_perfplotter_includes.push("js/lib/jquery.cookie.js");
_perfplotter_includes.push("js/perfplotter/PlotterBase.js");
_perfplotter_includes.push("js/perfplotter/Plotter.js");
_perfplotter_includes.push("js/perfplotter/Communicator.js");
_perfplotter_includes.push("js/perfplotter/SessionBase.js");

function _perfplotter_callback(data) {
    var session = new SessionBase(data['session']['id'], data['session'], data['data']);
    var plotterArray = _perfplotter_instances[session.getID()];
    $.each(plotterArray, function(idx, plotter){
        plotter.plot([session]);
    });
}

function _perfplotter_init (baseURL) {
    var mainCommunicator = new Communicator(baseURL);
    $(".pparea").each(function(idx){
        var rootElement = $(this);
        var sid = rootElement.attr("sid");
        var plotterArray = [];
        if (sid in _perfplotter_instances) {
            plotterArray = _perfplotter_instances[sid];
        }
        plotterArray.push(new Plotter(rootElement))
        _perfplotter_instances[sid] = plotterArray;
    });
    $(".pparea").each(function(idx){
        var sids = $(this).attr("sid") + "";
        $.each(sids.split("."), function(idx, sid){
            mainCommunicator.getSession(sid, null, null);
        });
    });
}

$(document).ready(function () {
    var baseURL = "";
    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }
    $('script').each(function(idx){
        var src = $(this).attr('src');
        if ( endsWith(src, _perfplotter_filename) ) {
            baseURL = src.replace(_perfplotter_filename, "")
        }
    });
    var deferredArray = [];
    $.each(_perfplotter_css, function(index, cssURL) {
        var css = $("<link>");
        css.appendTo($("head"));
        css.attr({
            rel : "stylesheet",
            type: "text/css",
            href: baseURL + cssURL
        });
    });
    $.each(_perfplotter_includes, function(index, include) {
        var deferred = $.getScript(baseURL + include);
        deferredArray.push(deferred);
    });
    $.when.apply($, deferredArray).done(function () {
        _perfplotter_init (baseURL);
    });
});