/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function MenuBar (rootElement, mainUI) {

    this.name = "MenuBar"

    this._html = function () {
        var authToggle = "";
        var adminButton = "";
        if (mainCommunicator.isAuthenticated()) {
            adminButton = "<div id='adminMenuItem' class='menuItem'>Admin</div>";
            authToggle = "Logout " + mainCommunicator.name
        } else {
            adminButton = "";
            authToggle = "Login"
        }
        return " \
            <div style='width: 30%; height: 100%; float: left;'>\
                <h1>Performance Plotter v0.1</h1>\
            </div> \
            <div id='menuItems' style='width: 70%; height: 100%; float: left;'>\
                <div id='toggleSubMenu' class='menuItem'><img src='img/graph.png'/></div> \
                <div id='authToggle' class='menuItem'>"+ authToggle + "</div> \
                <div id='apiMenuItem' class='menuItem'>API / About</div> \
                 " + adminButton + " \
                <div id='homeMenuItem' class='menuItem'>Home</div> \
            </div>";
    }

    this._onAdmin = function () {
        $.updateHash({ m: "admin" })
    }

    this._onHome = function () {
        $.updateHash({ m: "main" })
    }

    this._onAPI = function () {
        $.updateHash({ m: "api" })
    }

    this._onToggleSubMenu = function () {
        var subMenu = $('#submenu');
        if (subMenu.is(':visible')) {
            subMenu.hide();
        } else {
            subMenu.show();
        }
    }

    this._onTogglePlotType = function () {
        var hash = $.deserialize(window.location.hash);
        var toggleLabel = ( hash['type'] === "performance") ? "F/C" : "Cycles";
        hash['type'] = (hash['type'] === "performance") ? "cycles" : "performance";
        $.updateHash(hash);
        $('#togglePlotType').html(toggleLabel);
    }

    this._onToggleXScale = function () {
        var hash = $.deserialize(window.location.hash);
        var toggleLabel = ( hash['xscale'] === "log") ? "X Log" : "X Linear";
        hash['xscale'] = (hash['xscale'] === "log") ? "linear" : "log";
        $.updateHash(hash);
        $('#toggleXScale').html(toggleLabel);
    }

    this._onAuthToggle = function () {
        if (mainCommunicator.isAuthenticated()) {
            mainCommunicator.logout();
            window.location = window.location.pathname + "#sid=last&m=main"
            location.reload ();
        } else {
            window.location = window.location.pathname + "#m=admin"
        }
    }

    this._onExportPlotR = function () {
        if (mainUI !== undefined) {
            mainUI.exportPlot("R");
        }
    }

    this._initSubmenu = function () {
        var hash = $.deserialize(window.location.hash);
        if ( hash['m'] === "main") {
            var that = this;
            var toggleLabel = ( hash['type'] === "performance") ? "Cycles" : "F/C";
            var html = "<div id='submenu'> \
                <div id='togglePlotType' class='submenuItem'>" + toggleLabel + "</div> \
                <div id='toggleXScale'class='submenuItem'>X Log</div> \
                <div id='exportPlotR'class='submenuItem'>R</div> \
                <div id='exportPlotCSV'class='submenuItem'>CSV</div> \
            </div>";
            $(html).appendTo(rootElement);
            var submenu = $('#submenu');
            submenu.css({
                "margin-top"   : rootElement.height () + "px",
                "margin-left"  : rootElement.width () - submenu.width()  + "px"
            })
            console.log($('body').width () - submenu.width() + "px");
            $('#toggleXScale').click(that._onToggleXScale);
            $('#toggleSubMenu').click(that._onToggleSubMenu);
            $('#togglePlotType').click(that._onTogglePlotType);
            $('#exportPlotR').click(that._onExportPlotR);
        }
    }

    this._cleanSubmenu = function () {
        var hash = $.deserialize(window.location.hash);
        if ( hash['m'] === 'main') {
            $('#toggleSubMenu').off('click');
            $('#togglePlotType').off('click');
            $('#exportPlotR').off('click');
            $('#toggleXScale').off('click');
        }
    }

    this.init = function () {
        var that = this;
        $(that._html()).appendTo(rootElement);
        this._initSubmenu ();
        $('#authToggle').click(that._onAuthToggle);
        $('#homeMenuItem').click(that._onHome);
        $('#adminMenuItem').click(that._onAdmin);
        $('#apiMenuItem').click(that._onAPI);

    }

    this.clean = function () {
        this._cleanSubmenu ();
        $('#homeMenuItem').off('click')
        $('#adminMenuItem').off('click')
        $('#apiMenuItem').off('click');
        $('#aboutMenuItem').off('click');
        rootElement.empty();
    }

}