/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

package ch.ethz.pp.server

import ch.ethz.pp.db.DB._
import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import java.io._
import java.util.Date
import java.sql.Timestamp
import javax.servlet.http.{HttpServletResponse, HttpServletRequest, HttpServlet}
import ch.ethz.pp.db.DB

class SubmitServlet extends AuthenticationServler {

  def submitSession(json: JValue, uid: Int, response: HttpServletResponse) = {
    val id        = 0
    val title     = (json \ "title").extract[String]
    val commitID  = (json \ "commitID").extract[String]
    val timestamp = new Timestamp(System.currentTimeMillis / 1000)
    val maxPerf   = (json \ "maxPerf").extract[Double]
    val os        = (json \ "os").extract[String]
    val compiler  = (json \ "compiler").extract[String]
    val flags     = (json \ "flags").extract[String]
    val cpuModel  = (json \ "cpuModel").extract[String]
    val cpuFreq   = (json \ "cpuFreq").extract[Int]
    val ramSize   = (json \ "ramSize").extract[Int]
    val ramFreq   = (json \ "ramFreq").extract[Int]
    val opt       = (json \ "opt").extract[String]
    val s = Session(id, commitID, title, timestamp, uid, maxPerf, os, compiler, flags, cpuModel, cpuFreq, ramSize, ramFreq, opt)
    val jsonResponse = DB.insertSession(s).toJSON()
    response.getWriter().println(pretty(render(jsonResponse)))
    response.setStatus(HttpServletResponse.SC_OK)
  }

  def submitData(json: JValue, uid: Int, response: HttpServletResponse) = {
    val sid    = (json \ "sid").extract[Int]
    val xaxis  = (json \ "xaxis").extract[Double]
    val flops  = (json \ "flops").extract[Double]
    val cycles = (json \ "cycles").extract[Double]
    val series = (json \ "series").extract[String]
    val opt    = (json \ "opt").extract[String]
    val d = DataInstance(0, sid, xaxis, flops, cycles, series, opt)
    DB.insertDataInstance(d)
    response.setStatus(HttpServletResponse.SC_OK)
  }

  def deleteSession(json: JValue, uid: Int, response: HttpServletResponse) = {
    var status = HttpServletResponse.SC_OK
    DB.getSession((json \ "id").extract[Int]) match {
      case Some(s) => DB.deleteSession(s)
      case None => status = HttpServletResponse.SC_BAD_REQUEST
    }
    response.setStatus(status)
  }

  def updateSession(json: JValue, uid: Int, response: HttpServletResponse) = {
    var status = HttpServletResponse.SC_OK
    DB.getSession((json \ "id").extract[Int]) match {
      case Some(s) => {
        val title     = (json \ "title").extract[String]
        val commitID  = (json \ "commitID").extract[String]
        val maxPerf   = (json \ "maxPerf").extract[Double]
        val os        = (json \ "os").extract[String]
        val compiler  = (json \ "compiler").extract[String]
        val flags     = (json \ "flags").extract[String]
        val cpuModel  = (json \ "cpuModel").extract[String]
        val cpuFreq   = (json \ "cpuFreq").extract[Int]
        val ramSize   = (json \ "ramSize").extract[Int]
        val ramFreq   = (json \ "ramFreq").extract[Int]
        val opt       = (json \ "opt").extract[String]
        val updatedSession = Session(s.id, commitID, title, s.timestamp, s.uid, maxPerf, os, compiler, flags, cpuModel, cpuFreq, ramSize, ramFreq, opt)
        DB.updateSession(updatedSession)
      }
      case None => status = HttpServletResponse.SC_BAD_REQUEST
    }
    response.setStatus(status)
  }

  override def doPost(request: HttpServletRequest, response: HttpServletResponse ) = try {

    val json = getJSONRequest(request.getReader())
    val (isAllowed, user) = isUserAllowed(json)

    if ( !isAllowed ) {
      response.setStatus(HttpServletResponse.SC_FORBIDDEN)
    } else {
      (json \ "requestType").extract[String] match {
        case "updateSession" => updateSession (json, user.id, response)
        case "deleteSession" => deleteSession (json, user.id, response)
        case "session" => submitSession(json, user.id, response)
        case "data" => submitData(json, user.id, response)
        case _ => response.setStatus(HttpServletResponse.SC_BAD_REQUEST)
      }
    }
  } catch {
    case e: Exception => response.setStatus(HttpServletResponse.SC_BAD_REQUEST)
  }

}
