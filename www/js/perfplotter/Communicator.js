/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function Communicator (baseURL) {

    if ( typeof(baseURL) === 'undefined') baseURL = "";

    this._ppUsername = null;
    this._ppPassword = null;
    this.name = null;
    this.uid = null;

    this._createDeferred = function (f) {
        var deferred = $.Deferred();
        f();
        deferred.resolve();
        return deferred.promise();
    }

    this.isAuthenticated = function () {
        return this._ppUsername !== null && this._ppPassword !== null &&
            this._ppUsername !== undefined && this._ppPassword !== undefined
    }

    this.isCrossDomain = function () {
        return (baseURL !== "")
    }

    this.login = function (username, password) {
        var that = this;
        var authenticated = false;
        $.ajax({
            dataType: "json",
            async: false,
            type: "POST",
            url: baseURL + "plotter/login/",
            data: "{ \"username\" : \"" + username + "\", \"password\": \"" + password + "\" }",
            success: function (data) {
                $.cookie('ppUsername', username);
                $.cookie('ppPassword', password);
                that._ppUsername = username;
                that._ppPassword = password;
                that.name = data['name'];
                that.uid = data['id'];
                authenticated = true;
            }
        });
        return authenticated;
    }

    this.logout = function () {
        $.removeCookie('ppUsername');
        $.removeCookie('ppPassword');
        this._ppUsername = null;
        this._ppPassword = null;
        this.name = null;
    }

    this.autoLogin = function () {
        var username = $.cookie('ppUsername');
        var password = $.cookie('ppPassword');
        if ( username !== undefined && password !== undefined) {
            if ( !this.login(username, password) ) {
                $.removeCookie('ppUsername');
                $.removeCookie('ppPassword');
            }
        }
    }

    if ( !this.isCrossDomain() ) {
        this.autoLogin ();
    }

    this.getSession = function (sid, successF, errorF) {
        if (this.isCrossDomain()) {
            return $.ajax({
                dataType: "jsonp",
                url: baseURL + "plotter/resultsp/?session=" + sid
            });
        } else {
            return $.ajax({
                dataType: "json",
                url: baseURL + "plotter/results/?session=" + sid,
                success: successF,
                error: errorF
            });
        }
    }

    this.deleteSession = function (sid, successF, errorF) {
        if (mainCommunicator.isAuthenticated()) return $.ajax({
            type: 'POST',
            data: JSON.stringify({
                requestType : "deleteSession",
                id          : sid,
                username    : mainCommunicator._ppUsername,
                password    : mainCommunicator._ppPassword
            }),
            url: baseURL + "plotter/submit/",
            success: successF,
            error: errorF
        }); else {
            return this._createDeferred(errorF);
        }
    }

    this.updateSession = function (s, successF, errorF) {
        if (mainCommunicator.isAuthenticated()) {
            var session = $.extend({}, s);
            $.extend (session, {
                requestType : "updateSession",
                username    : mainCommunicator._ppUsername,
                password    : mainCommunicator._ppPassword
            });
            return $.ajax({
                type: 'POST',
                data: JSON.stringify(session),
                url: baseURL + "plotter/submit/",
                success: successF,
                error: errorF
            });
        } else {
            return this._createDeferred(errorF);
        }
    }



}