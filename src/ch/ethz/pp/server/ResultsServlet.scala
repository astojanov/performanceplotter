/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */
package ch.ethz.pp.server

import javax.servlet.http.{HttpServletResponse, HttpServletRequest, HttpServlet}
import ch.ethz.pp.db.DB
import net.liftweb.json._
import net.liftweb.json.JsonDSL._

class ResultsServlet extends HttpServlet {

  def getAllSessionsJSON(): Option[JObject] = {
    Some("sessions" -> DB.getAllSessions().map(s => {
      DB.getUserByID(s.uid) match {
        case Some(user) => s.toJSON() ~ ("userFullName" -> user.name)
        case None => s.toJSON()
      }
    }))
  }

  def getSessionJSON (id: Int) = DB.getSession(id) match {
    case Some(session) => {
      val sessionJSON = DB.getUserByID(session.uid) match {
        case Some(user) => session.toJSON() ~ ("userFullName" -> user.name)
        case None => session.toJSON()
      }
      Some(("session" -> sessionJSON) ~
        ("data" -> DB.getDataInstancesBySID(session.id).map(_.toJSON())))
    }
    case None => None
  }

  def getLastSessionJSON () = DB.getLastSession() match {
    case Some(session) => {
      Some(("session" -> session.toJSON()) ~
        ("data" -> DB.getDataInstancesBySID(session.id).map(_.toJSON())))
    }
    case None => None
  }

  protected def printResponse (response: HttpServletResponse, json: JObject) = {
    response.getWriter().println(pretty(render(json)))
    response.setStatus(HttpServletResponse.SC_OK)
  }

  override def doGet(request: HttpServletRequest, response: HttpServletResponse) = {
    val session = request.getParameter("session")
    if ( session == null ) {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST)
    } else {
      response.setContentType("application/json")
      val jsonOption: Option[JObject] = session match {
        case "all"  => getAllSessionsJSON ()
        case "last" => getLastSessionJSON ()
        case _ => getSessionJSON(Integer.parseInt(session))
      }
      jsonOption match {
        case Some(json) => printResponse(response, json)
        case None => response.setStatus(HttpServletResponse.SC_BAD_REQUEST)
      }
    }
  }
}
