/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function SessionBase (sid, c_session, c_data) {

    this.name         = "session_" + sid;

    this._session = (c_session === undefined) ? null : c_session;
    this._data    = (c_data === undefined) ? [] : c_data;
    this._series  = null;
    this._jqXHR   = null;

    this._sessionFieldTypes = [
        { label: 'Plot Title',          id: 'title',    type: 'string' },
        { label: 'Commit ID',           id: 'commitID', type: 'string' },
        { label: 'Maximum Performance', id: 'maxPerf',  type: 'float'  },
        { label: 'Operating System',    id: 'os',       type: 'string' },
        { label: 'Compiler',            id: 'compiler', type: 'string' },
        { label: 'Compiler Flags',      id: 'flags',    type: 'string' },
        { label: 'CPU Model',           id: 'cpuModel', type: 'string' },
        { label: 'CPU Frequency',       id: 'cpuFreq',  type: 'int'    },
        { label: 'RAM Size',            id: 'ramSize',  type: 'int'    },
        { label: 'RAM Frequency',       id: 'ramFreq',  type: 'int'    },
        { label: 'Optional Info',       id: 'opt',      type: 'string' }
    ];

    this._convertTimestamp = function (timestamp) {
        return (new Date(timestamp * 1000)).toString();
    }

    this.getID = function () {
        return sid;
    }

    this.getField = function (field) {
        return this._session[field];
    }

    this.getSession = function () {
        return this._session;
    }

    this.getData = function () {
        return this._data
    }

    this.merge = function (session) {
        this._data = session.getData();
    }

    this.loadData = function () {
        var that = this;
        if ( that._data.length === 0) {
            that._jqXHR = mainCommunicator.getSession (sid, function (data){
                that._session = data['session'];
                that._data = data['data'];
                that._checkSessionValidity(that._session);
            }, function () {});
            return that._jqXHR;
        } else {
            var deferred = $.Deferred();
            deferred.resolve();
            return deferred; //.promise();
        }
    }

    this.getSeries = function () {
        if (this._series === null) {
            var series = [];
            var map = {};
            $.each(this._data, function(idx, dataInstance) {
                var sName = dataInstance['series'];
                if (!(sName in map)) {
                    series.push(sName)
                    map[sName] = series.length - 1;
                }
            });
            this._series = series;
        }
        return this._series;
    }

    this._clean = function () {
        if ( this._jqXHR !== null ) {
            this._jqXHR.abort();
        }
        this._session = null;
        this._data    = null;
        this._series  = null;
        this._jqXHR   = null;
    }

    this._checkSessionValidity = function(session) {
        var validSessionObject = true;
        $.each(this._sessionFieldTypes, function(idx, field) {
            if ( session[field.id] === undefined ) {
                validSessionObject = false;
            }
        });
        if (!validSessionObject) {
            console.error("Object Specified is not a valid session object")
        }
        return validSessionObject;
    }

}