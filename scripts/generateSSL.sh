openssl genrsa -des3 -out ../conf/pp.key 2048
openssl req -new -x509 -key ../conf/pp.key -out ../conf/pp.crt
keytool -keystore ../conf/ppkeystore -import -alias pp -file ../conf/pp.crt -trustcacerts
openssl req -new -key ../conf/pp.key -out ../conf/pp.csr
openssl pkcs12 -inkey ../conf/pp.key -in ../conf/pp.crt -export -out ../conf/pp.pkcs12
keytool -importkeystore -srckeystore ../conf/pp.pkcs12 -srcstoretype PKCS12 -destkeystore ../conf/ppkeystore
