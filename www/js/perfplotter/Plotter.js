/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function Plotter (chartElement) {

    $.extend(this, new PlotterBase ());

    this._chartElement = chartElement;

    this._xScale = "linear";

    this.setXScale = function (scale) {
        this._xScale = scale;
    }

    this._widthSizes = [
        { maxWidth:  600, sizes: { title: '16px', subtitle: '12px', axis: '12px', labels: '10px', plotLineWidth: 3 } },
        { maxWidth: 1000, sizes: { title: '20px', subtitle: '16px', axis: '16px', labels: '12px', plotLineWidth: 9 } }
    ];

    this.plot = function (sessions) {
        var plot    = this._mergeSessions (sessions);
        var series  = this._mergeSeries   (sessions);
        this._renderPlot(plot, series);
    }


    this._matchSizesToWidth = function () {
        var sizes = this._widthSizes[0].sizes;
        for ( var i = 1; i < this._widthSizes.length; i++ ) {
            if (chartElement.width() > this._widthSizes[i-1].maxWidth) {
                sizes = this._widthSizes[i].sizes;
            }
        }
        return sizes;
    }

    this._fontStyle = function (section) {
        return {
            fontFamily: '\'Lato\', sans-serif',
            fontSize: this._matchSizesToWidth () [section],
            fontWeight: 'bold',
            color: '#000000'
        }
    }

    this._renderPlot = function (plotInfo, series) {
        var that = this;
        var settings = {
            credits: {
                enabled: false
            },
            chart: {
                type: 'line',
                plotBackgroundColor: "#ebeced",
                zoomType: 'xy'
            },
            title: {
                align: "left",
                text: plotInfo['title'],
                x: 30,
                style: that._fontStyle('title'),
                margin: 20
            },
            subtitle: {
                align: "left",
                text: plotInfo['subtitle'],
                x: 30,
                style: that._fontStyle('subtitle')
            },
            xAxis: {
                title: {
                    text: 'Size',
                    style: that._fontStyle('axis')
                },
                lineColor: '#000000',
                lineWidth: 2,
                labels: {
                    style: that._fontStyle('labels')
                }
            },
            yAxis: {
                title: {
                    text: '',
                    style: that._fontStyle('axis')
                },
                labels: {
                    style: that._fontStyle('labels')
                },
                gridLineColor: '#FFFFFF',
                gridLineWidth: 3
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: series
        };


        console.log("the scale is: ", this._xScale);

        if (this._xScale === "log") {
            settings.xAxis.type = 'logarithmic';
        }
        if (this._mode === "performance") {
            // Define bounds for min / max for performance
            settings.yAxis.min = 0;
            settings.yAxis.max = parseFloat(plotInfo['maxPerf']) * 1.1;
            // Draw the lines for performance
            settings.yAxis.plotLines = [{
                value: parseFloat(plotInfo['maxPerf']),
                width: this._matchSizesToWidth()['plotLineWidth'],
                color: '#800000'
            }];
            settings.yAxis.title.text = 'Performance F/C';
            settings.tooltip = {
                pointFormat: "F/C: <b>{point.y}</b><br/>Optional:<br/><b>{point.opt}</b>"
            }
        } else if (this._mode === "cycles") {
            settings.yAxis.title.text = 'Cycles';
            settings.tooltip = {
                pointFormat: "Cycles: <b>{point.y}</b><br/>Optional:<br/><b>{point.opt}</b>"
            }
        }

        this._chartElement.highcharts(settings);
    }
}
