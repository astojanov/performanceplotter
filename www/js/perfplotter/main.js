/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

var currentHash      = {};
var previousHash     = {};
var currentModule    = null;
var mainCommunicator = new Communicator ();

function changePlotterModule (module) {
    if ( currentModule !== null && currentModule.name === module ) {
        currentModule.hashChange();
    } else {
        if (currentModule !== null) {
            currentModule.clean ();
        }
        if (module !== undefined) {
            if (module === "main") {
                currentModule = new MainUI($('body'));
            } else if (module === "admin") {
                currentModule = new AdminUI($('body'));
            } else if (module === "api") {
                currentModule = new APIUI($('body'));
            }
        } else {
            currentModule = new MainUI($('body'));
        }
        currentModule.init();
    }
}

$(document).ready(function () {
    var hashChange = function () {
        previousHash = currentHash;
        var hash = $.deserialize(window.location.hash);
        currentHash = hash;
        changePlotterModule(hash['m']);
    }
    currentHash = $.deserialize(window.location.hash)
    previousHash = $.deserialize(window.location.hash)
    window.onhashchange = hashChange
    hashChange();
});