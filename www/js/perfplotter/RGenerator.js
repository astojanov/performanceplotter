/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function RGenerator () {

    $.extend(this, new PlotterBase ());

    this._getSeriesVar = function (idx, sid) {
        return "serie_" + idx + "_" + sid;
    }

    this._getPlotDataFrameVar = function(plot) {
        return "plot_df_" + plot.sids;
    }

    this._getPlotVar = function(plot) {
        return "plot_" + plot.sids;
    }

    this._generateSeries = function (plot, series) {

        var str = "";

        var data2R  = function(data, f) {
            var str = "c(" + data[0][f];
            for (var i = 1; i < data.length; i++) str += ", " + data[i][f];
            str += ")";
            return str;
        }
        var serie2R = function(serie) {
            return "data.frame(x=" + data2R(serie.data, "x") + ", y=" + data2R(serie.data, "y") + ", series=\"" + serie.name + "\")";
        }

        for (var i = 0; i < series.length; i++) {
            str += this._getSeriesVar(i, plot.sids) + " <- " + serie2R(series[i]) + "; \n";
        }

        return str;
    }

    this.generate = function (sessions) {

        var str     = "";
        var plot    = this._mergeSessions (sessions);
        var series  = this._mergeSeries   (sessions);

        var seriesR = this._generateSeries(plot, series);
        var plotR =  this._getPlotDataFrameVar(plot) + " <- do.call(rbind, list(" + this._getSeriesVar(0, plot.sids);
        for (var i = 1; i < series.length; ++i) {
            plotR += ", " + this._getSeriesVar(i, plot.sids);
        }
        plotR += "));";

        str += 'install.packages("ggplot2")\n';
        str += 'library(ggplot2) \n\n';

        str += 'getLegend <- function() { return(theme(legend.position="right")) } \n';
        str += 'getTitle <- function(tmp) { return(ggtitle(tmp)) } \n';
        str += 'getAxisTextElement <- function() { return(element_text(hjust=0.5, size=8, colour="black")) } \n';
        str += 'getAxisTitleElement <- function() { return(element_text(hjust=0.5, size=10)) } \n';
        str += 'getTitleElement <- function () { return(element_text(hjust=0, size=10)) } \n';
        str += 'colorPalette <- c("#000000", "#585858", "#585858", "#585858", "#585858", "#585858", "#585858") \n';
        str += 'shapePalette <- c(16, 23, 24, 25, 22) \n';
        str += 'shapeSizes <- c(0, 2, 2, 2, 2, 2) \n\n\n';

        str += seriesR + '\n';
        str += plotR + '\n\n';

        str += this._getPlotVar(plot);

        str += ' <- ggplot(' + this._getPlotDataFrameVar(plot) + ', aes(x, y, colour=series, shape=series)) + \n';
        str += '\t getTitle("' + plot.title + '") + \n';
        str += '\t xlab("Input size") + \n';
        if (this._mode === "performance") {
            str += '\t coord_cartesian(ylim = c(0, ' + (plot.maxPerf + 1) + ')) + \n';
            str += '\t scale_y_continuous(limits = c(0, ' + plot.maxPerf + ')) + \n';
            str += '\t geom_hline(yintercept=' + plot.maxPerf + ', size=1, color="#585858") + \n'
        }
        str += '\t geom_line() + \n';
        str += '\t geom_point(aes(size=series), fill = "white") + \n';
        str += '\t theme( \n';
        str += '\t \t panel.grid.major.x = element_blank(),\n';
        str += '\t \t panel.grid.minor.y = element_blank(),\n';
        str += '\t \t panel.grid.minor.x = element_blank(),\n';
        str += '\t \t legend.title=element_blank(),\n';
        str += '\t \t axis.title.y=element_blank(),\n';
        str += '\t \t axis.line = element_line(colour = "white"),\n';
        str += '\t \t axis.line.x = element_line(colour = "black"),\n';
        str += '\t \t axis.text = getAxisTextElement(),\n';
        str += '\t \t axis.title = getAxisTitleElement(),\n';
        str += '\t \t plot.title = getTitleElement()\n';
        str += '\t ) + \n';

        str += '\t getLegend() + \n';
        str += '\t scale_colour_manual(values=colorPalette) + \n';
        str += '\t scale_shape_manual(values=shapePalette) + \n';
        str += '\t scale_size_manual(values=shapeSizes) \n';

        str += '\n' + this._getPlotVar(plot) + '\n';

        console.log(str);

        return str;
        // this._renderPlot(plot, series);
    }

}