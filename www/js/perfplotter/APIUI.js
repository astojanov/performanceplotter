/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function APIUI (rootElement) {

    this.name = "api";
    this._menuBar = null;

    this._html = " \
        <div id='menubar'></div> \
        <div id='sidebar'><div id='aboutContainer'></div></div> \
        <div id='mainContainer'> \
                <div id='mainAPIContainer'></div>\
        </div>";

    this._hashInit = function () {
        var hash = {m: "api"}
        $.updateHash(hash);
    }

    this.init = function () {
        var that = this;
        $(this._html).appendTo(rootElement);
        this._menuBar = new MenuBar($('#menubar'))
        this._menuBar.init();
        this._hashInit();
        this._initSize ();
        var d1 = $.get('api.html').success(function(data) {
            $('#mainAPIContainer').html(data);
        });
        var d2 = $.get('about.html').success(function(data) {
            $('#aboutContainer').html(data);
        });
        $.when.apply($, [d1, d2]).done(function () {
            that._onWindowResize ();
        });

    }

    this._onWindowResize = function () {
        var w = $('body').width() - $("#sidebar").outerWidth();
        var h = $('body').height() - $('#menubar').outerHeight();
        $('#mainContainer').css({
            "width"      : w + "px",
            "min-height" : h + "px"
        });
        $('#sidebar').css({
            "margin-top" : $('#menubar').outerHeight() + "px",
            "height" : h + "px"
        });
    }


    this._initSize = function  () {
        $('#mainContainer').css({ "position" : "absolute" });
        $('#sidebar').css({ "position" : "fixed" });
        $(window).resize(this._onWindowResize)
    }

    this.hashChange = function () {

    }

    this.clean = function () {
        this._menuBar.clean();
        rootElement.empty ();
    }
}