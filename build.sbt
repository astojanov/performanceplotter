name := "PP"

version := "0.1"

scalaSource in Compile <<= baseDirectory(_ / "src")

scalaSource in Test <<= baseDirectory(_ / "test-src")

resourceDirectory in Compile <<= baseDirectory(_ / "conf")

resourceDirectory in Test <<= baseDirectory(_ / "conf")

scalaVersion := "2.10.0"

libraryDependencies += "org.slf4j" % "slf4j-nop" % "1.6.4"

libraryDependencies += "com.typesafe" % "config" % "1.0.2"

libraryDependencies += "com.typesafe.slick" %% "slick" % "2.0.1-RC1"

libraryDependencies += "org.xerial" % "sqlite-jdbc" % "3.7.2"

libraryDependencies += "org.atteo.moonshine" % "jetty" % "0.9"

libraryDependencies += "net.liftweb" %% "lift-json" % "2.6-M2"

