/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function AdminSessionUI (rootElement, session) {

    $.extend(this, new SessionBase (session['id']));

    this._session = session;
    this._menuItems = [];
    this._plotter = null;

    this._html = function () {
        var that = this;
        var sid = that.getID(); // that._session['id'];
        var s = that._session;
        var fields = "";
        $.each(that._sessionFieldTypes, function(idx, fieldType){
            fields += "<tr><td class='sessonFieldLabel'>" + fieldType.label + "</td><td>";
            fields += "<input id='" + fieldType.id + "_" + sid + "' type='text' value='" + s[fieldType.id] + "'></td></tr>";
        })
        return "<table style='width: 100%;'>\
                <tr>\
                    <td> \
                        <p><b id='label_title_" + sid + "'>" + s["title"] + "</b></p> \
                        <p>" + this._convertTimestamp(s["timestamp"]) + "</p> \
                        <p id='label_cpuinfo_" + sid + "'>" + s["cpuModel"] + ' ' + s["cpuFreq"] + 'MHz, ' + s["compiler"] + "</p>\
                        <p id='label_commitID_" + sid + "'>" + s["commitID"] + "</p> \
                    </td><td> \
                        <a href='#' id='edit_" + sid + "'><img src='img/edit.png'></img></a> \
                        </td><td> \
                        <a href='#' id='delete_" + sid + "'><img src='img/delete.png'></img></a> \
                    </td>\
                </tr><tr>\
                    </tr><td class='sessionEditContainer' id='sessionEditContainer_" + sid + "' colspan='3'> \
                        <table style='padding:0; border-collapse: collapse; width: 100%'>\
                        <tr><td colspan='2' id='plotterArea_" + sid + "' style='width: 100%; height: 337px;'></td></tr>" +
                        "" + fields + "<tr><td colspan='2' style='text-align: center'>\
                                <input style='margin-top: 10px; width: 100px;' id='update_" + sid + "' type='submit' value='Update'> &nbsp;\
                                <input style='margin-top: 10px; width: 100px;' id='update_cancel_" + sid + "' type='submit' value='Cancel'>\
                            </td></tr>\
                        </table>\
                    </td>\
                </tr>\
            </table>";
        return s;
    }

    this._onDelete = function (e, that) {
        e.preventDefault();
        var session = that._session;
        var conf = confirm("Are you sure you want to delete \"" + session['title'] + "\"");
        if (conf === true){
            rootElement.hide("slow");
            mainCommunicator.deleteSession(session['id'], function () {
                that.clean();
                rootElement.remove();
            },
            function () {
                alert("Session: " + session['title'] + " can not be deleted");
                rootElement.show("slow");
            });
        }
    }

    this._onEdit = function (e, that) {
        e.preventDefault();
        var session = that._session;
        if ( $('#sessionEditContainer_' + session['id']).css("display") === "none") {
            var d1 = that.loadData ();
            $('#sessionEditContainer_' + session['id']).show("slow", function(){
                d1.done(function () { that._plotter.plot([that]); });
            });
        } else {
            $('#sessionEditContainer_' + session['id']).hide("slow");
        }
    }

    this._onUpdate = function (e, that) {

        e.preventDefault();

        // clone object first
        var session = $.extend({}, that._session);
        var sid = session['id'];

        var updateFields = function (map) {
            $.each(that._sessionFieldTypes, function(idx, fieldType) {
                var value = $("#" + fieldType.id + "_" + sid).val()
                if (fieldType["type"] === "int") {
                    value = parseInt(value)
                } else if (fieldType["type"] === "float") {
                    value = parseFloat(value)
                }
                map[fieldType.id] = value
            });
        }
        updateFields (session);

        mainCommunicator.updateSession(session, function () {
            $("#label_title_" + sid).html(session['title']);
            $("#label_cpuinfo_" + sid).html(session["cpuModel"] + ' ' + session["cpuFreq"] + 'MHz, ' + session["compiler"]);
            $("#label_commitID_" + sid).html(session['commitID']);
            updateFields(that._session);
            that._plotter.plot([that]);
        }, function () {
            alert("Session: " + that._session['title'] + " can not be updated");
        })

    }

    this.init = function () {
        var that = this;
        var session = that._session;
        $(this._html()).appendTo(rootElement);
        $('#edit_' + session['id']).click(function(e) { that._onEdit(e, that) });
        $('#delete_' + session['id']).click(function(e) { that._onDelete(e, that) });
        $('#update_' + session['id']).click(function(e) { that._onUpdate(e, that) });
        $('#update_cancel_' + session['id']).click(function(e) {
            e.preventDefault();
            $('#edit_' + session['id']).trigger("click");
        });
        this._plotter = new Plotter ($('#plotterArea_' + session['id']));
    }

    this.clean = function () {
        var session = that._session;
        $('#edit_' + session['id']).off('click');
        $('#delete_' + session['id']).off("click");
        $('#update_' + session['id']).off("click");
        $('#update_cancel_' + session['id']).off("click");
        rootElement.empty();
    }

}