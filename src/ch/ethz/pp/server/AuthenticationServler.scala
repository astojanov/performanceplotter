/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

package ch.ethz.pp.server

import ch.ethz.pp.db.DB._
import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import java.io._
import java.util.Date
import java.sql.Timestamp
import javax.servlet.http.{HttpServletResponse, HttpServletRequest, HttpServlet}
import ch.ethz.pp.db.DB

class AuthenticationServler extends HttpServlet {

  implicit object formats extends Formats {
    val dateFormat: DateFormat = new DateFormat {
      def parse(s: String): Option[Date] = Some(new Date())
      def format(d: Date): String = d.toString
    }
  }

  protected def getJSONRequest (reader: BufferedReader) = {
    var jb = ""
    var line: String = reader.readLine()
    while (line != null) {
      jb = jb + "\n" + line
      line = reader.readLine();
    }
    JsonParser.parse(jb)
  }

  protected def isUserAllowed (json: JValue): (Boolean, User) = {
    val username = (json \ "username").extract[String]
    val password = (json \ "password").extract[String]
    DB.getUserByUsername(username) match {
      case Some(user) => (user.password == password, user)
      case None => (false, null)
    }
  }

  override def doPost(request: HttpServletRequest, response: HttpServletResponse ) = try {
    val json = getJSONRequest(request.getReader())
    val (isAllowed, user) = isUserAllowed(json)
    if ( !isAllowed ) {
      response.setStatus(HttpServletResponse.SC_FORBIDDEN)
    } else {
      response.getWriter().println(pretty(render(user.toJSON())))
      response.setStatus(HttpServletResponse.SC_OK)
    }
  } catch {
    case e: Exception => response.setStatus(HttpServletResponse.SC_BAD_REQUEST)
  }

}
