/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function MainUI (rootElement) {

    this.name = "main";
    this._hash = {};

    this._menuBar = null;
    this._sessions = {};
    this._plotter = null;

    this._plotterMode = null;
    this._plotterXScale = null;

    this._baseSessions = {};
    this._lastSessionID = null;
    this._allSessions_jqXHR = null;

    this._html = " \
        <div id='menubar'></div> \
        <div id='searchBox' class='frame'> \
            <input type='text' id='search' class='search-input' placeholder='Search...' /> \
         </div> \
        <div id='sidebar'>\
            <div id='sessions'></div>\
        </div> \
        <div id='mainContainer'> \
            <div class='outer'><div class='middle'> \
                <div id='chartContainer'></div> \
            </div></div>\
        </div>";


    this._onSearch = function (that, searchField) {
        var search = $.trim(searchField.val());
        if (search === "") {
            $.each(that._sessions, function(idx, session) {
                session.show ();
            });
        } else {

            var searchTerms = $.grep(search.toLowerCase().split(" "), function(idx, term) {
                return ($.trim(term) !== "");
            });
            $.each(that._sessions, function(idx, session) {
                if (session.search(searchTerms)) {
                    session.show ();
                } else {
                    session.hide ();
                }
            });
        }
    }

    this.init = function () {
        var that = this;
        $(this._html).appendTo(rootElement);
        this._plotter = new Plotter($('#chartContainer'));
        this._menuBar = new MenuBar($('#menubar'), this);
        this._menuBar.init();
        this._initSize ();
        $.when(this.hashChange(), this.getAllSessions ()).done(function () {
            that._mergeBaseSessions();
            $('#search').keyup(function(e){ that._onSearch(that, $(this)); });
        })
    }

    this.hashChange = function () {

        var that = this;
        var shouldUpdate = false;
        var hash = $.deserialize(window.location.hash);

        var sid    = (hash['sid'])    ? hash['sid']    : 'last';
        var type   = (hash['type'])   ? hash['type']   : 'performance';
        var xscale = (hash['xscale']) ? hash['xscale'] : 'linear';
        var m = "main";

        if (sid === "last" && that._lastSessionID !== null) {
            sid = that._lastSessionID;
        }

        shouldUpdate = shouldUpdate || (sid    !== this._hash['sid']);
        shouldUpdate = shouldUpdate || (type   !== this._hash['type']);
        shouldUpdate = shouldUpdate || (xscale !== this._hash['xscale']);

        if (!$.isEmptyObject($.objectDiff(hash, that._hash))) {
            this._hash = { sid: sid, type: type, xscale: xscale, m: m };
            $.updateHash(this._hash)
        }

        if (shouldUpdate) {
            return this._updatePlotter();
        } else {
            var deferred = $.Deferred();
            deferred.resolve();
            return deferred.promise();
        }
    }

    this._getHashSessionsIDs = function () {
        var sids = [];
        var that = this;
        $.each((this._hash['sid']+"").split("."), function(idx, sid) {
            if ($.isNumeric(sid)) {
                sids.push(parseInt(sid));
            } else if (sid === "last"){
                sids.push(that._getLastID());
            }
        });
        return $.unique(sids);
    }

    this._serializeSessionIDs = function (sids) {
        return $.unique(sids.sort()).join(".");
    }

    this._getLastID = function () {
        if (this._lastSessionID !== null) {
            return this._lastSessionID;
        } else {
            return "last";
        }
    }

    this._mergeBaseSessions = function () {
        var that = this;
        $.each(that._baseSessions, function(idx, baseSession){
            var sid = baseSession.getID();
            if (sid === "last") sid = that._getLastID();
            if (sid in that._sessions) {
                that._sessions[sid].merge(baseSession);
            }
        });
        $.each(that._getHashSessionsIDs (), function(idx, sid){
            if (sid === "last") sid = that._getLastID();
            if (sid in that._sessions) {
                that._sessions[sid].setActive(true);
            }
        });
        if ("last" in  that._sessions) {
            delete that._sessions["last"];
        }
        that._baseSessions = {};
    }

    this._updatePlotter = function () {

        var that = this;
        that._plotterMode   = that._hash['type'];
        that._plotterXScale = that._hash['xscale'];
        var sessions = [];
        var deferred = [];

        $.each(that._sessions, function(idx, session){
            session.setActive(false);
        });

        $.each(that._getHashSessionsIDs (), function(idx, sid){
            var session = null;
            if (sid in that._sessions) {
                session = that._sessions[sid];
                session.setActive(true);
            } else {
                session = new SessionBase(sid);
                that._baseSessions[sid] = session;
            }
            sessions.push(session);
            deferred.push(session.loadData());
        });

        var promise = $.when.apply($, deferred);
        promise.done(function () {
            that._plotter.setXScale(that._plotterXScale);
            that._plotter.setMode(that._plotterMode);
            that._plotter.plot(sessions);
        })
        return promise;
    }

    this._initSize = function  () {
        $('#mainContainer').css({ "position" : "fixed" });
        var onWindowResize = function () {
            var w = $('body').width()  - $("#sidebar").outerWidth ();
            var h = $('body').height() - $('#menubar').outerHeight();
            $('#mainContainer').css({
                "width"  : w + "px",
                "height" : h + "px"
            });
            $('#sidebar').css({
                "margin-top" : $('#searchBox').outerHeight() + $('#menubar').outerHeight() + "px",
                "min-height" : h - $('#searchBox').outerHeight() + "px"
            });
            if ( 9 * w < 16 * h) {
                w = w - 20;
                h = Math.floor(9.0 * w / 16.0);
            } else {
                h = h - 20;
                w = Math.floor(16.0 * h / 9.0);
            }
            $('#chartContainer').css({
                "width"  : (w + "px"),
                "height" : (h + "px")
            });
        }
        $(window).resize(onWindowResize)
        onWindowResize ();
    }

    this._getActiveSessions = function () {
        var that = this;
        var sessions = [];
        $.each(that._getHashSessionsIDs (), function(idx, sid){
            if (sid in that._sessions) {
                var session = that._sessions[sid];
                sessions.push(session);
            }
        });
        return sessions;
    }

    this.exportPlot = function (plotType) {

        var exporter = null;
        var sessions = this._getActiveSessions();

        if (plotType == "R") {
            exporter = new RGenerator ();
        }

        exporter.setMode(this._plotterMode);
        var code = exporter.generate(sessions);

        var width  = Math.round($(window).width() * 0.8);
        var height = Math.round($(window).height() * 0.8);
        $.colorbox({html: "<pre>" + code + "</pre>", width: width, height: height});
    }

    this.showSession = function (sid, append) {
        append = (append === undefined) ? false : append;
        var hash = $.extend({}, this._hash);
        var sids = [];
        if ( append ) sids = this._getHashSessionsIDs();
        sids.push(sid);
        hash['sid'] = this._serializeSessionIDs(sids);
        $.updateHash(hash);
    }

    this.pinSession = function(sid) {
        this.showSession(sid, true);
    }

    this.unpinSession = function (sid) {
        var hash = $.extend({}, this._hash);
        var sids = $.grep(this._getHashSessionsIDs(), function(value) {
            return value !== sid;
        });
        hash['sid'] = this._serializeSessionIDs(sids);
        $.updateHash(hash);
    }

    this.getAllSessions = function () {
        var that = this;
        this._allSessions_jqXHR = mainCommunicator.getSession("all",  function (data) {
            var list = $('<ul/>');
            list.appendTo($('#sessions'));
            if ( data['sessions'].length > 1)
                that._lastSessionID = data['sessions'][0]['id'];
            $.each(data['sessions'], function (idx, session) {
                that._sessions[session['id']] = new SessionUI(list, that, session);
                that._sessions[session['id']].init()
            });
        });
        return this._allSessions_jqXHR;
    }

    this.clean = function () {
        $("#search").off("keyup");
        if (this._allSessions_jqXHR !== null) {
            this._allSessions_jqXHR.abort();
        }
        $.each(this._baseSessions, function(idx, session) { session._clean(); });
        $.each(this._sessions, function(idx, session) { session.clean(); });
        this._menuBar.clean();
        this._menuBar = null;
        this._sessions = {};
        this._plotter = null;
        this._plotterMode = null;
        this._baseSessions = {};
        this._lastSessionID = null;
        rootElement.empty ();

    }
}