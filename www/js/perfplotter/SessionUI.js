/**
 *      ____               ____ ____   __        __   __
 *     / __ \ ___   _____ / __// __ \ / /____   / /_ / /_ ___   _____
 *    / /_/ // _ \ / ___// /_ / /_/ // // __ \ / __// __// _ \ / ___/
 *   / ____//  __// /   / __// ____// // /_/ // /_ / /_ /  __// /
 *  /_/     \___//_/   /_/  /_/    /_/ \____/ \__/ \__/ \___//_/
 *
 *  https://bitbucket.org/astojanov/performanceplotter/
 *  PerformancePlotter 0.1 - ETH Zurich
 *  Copyright (C) 2014  Alen Stojanov  (astojanov@inf.ethz.ch)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see http://www.gnu.org/licenses/.
 */

function SessionUI (parentElement, mainUI, session) {

    $.extend(this, new SessionBase (session['id']));

    this._session = session;
    this._checkSessionValidity(session);

    this._rootElement = $('<li/>');
    this._active = false;

    this.isActive = function () {
        return this._active;
    }

    this.show = function () {
        this._rootElement.show ();
    }

    this.hide = function () {
        this._rootElement.hide ();
    }

    this.search = function (allTerms) {
        var terms = allTerms.slice();
        var size = terms.length;
        var findTerm = function (value) {
            for (var j = 0; j < size && size > 0; j++) {
                if (value.indexOf(terms[j]) !== -1) {
                    terms[j] = terms[size - 1]
                    size = size - 1;
                    j = j - 1;
                }
            }
        }
        for (var i = 0; i < this._sessionFieldTypes.length && size > 0; i++) {
            var fieldID = this._sessionFieldTypes[i].id;
            var value   = this.getField(fieldID) + "";
            findTerm(value.toLowerCase());
        }
        if (size > 0) findTerm(this._session['userFullName'].toLowerCase());
        return (size <= 0);
    }


    this.setActive = function (active) {
        if (active) {
            $('#session_title_' + this.getID()).css("color", "#FAFAD2");
            $('#session_pin_' + this.getID()).attr("src", "img/selected.png");
        } else {
            $('#session_title_' + this.getID()).css("color", "#FFFFFF");
            $('#session_pin_' + this.getID()).attr("src", "img/select.png");
        }
        this._active = active;
    }

    this._html = function () {
        var that = this;
        var s = '<h3 id="session_title_' + that.getID() + '">' + that._session['title'] + '</h3>';
        s += '<p>' + that._session['userFullName'] + '</p>';
        s += '<p>' + that._convertTimestamp(session['timestamp']) + '</p>';
        s += '<p>' + that._session['cpuModel'] + " " + that._session['cpuFreq'] + "MHz, ";
        s += that._session['compiler'] + '</p>';
        var table = '<table style="width: 100%"><tr>\
            <td>' + s + '</td>\
            <td style="width: 48px;"><img id="session_pin_' + that.getID() + '" src="img/select.png"></img></td>\
        </tr></table>';

        return table;

    }

    this.init = function () {
        var that = this;
        this._rootElement.appendTo(parentElement);
        this._rootElement.html(this._html());
        this._rootElement.click (function (e) {
            mainUI.showSession(that._session['id'])
        });
        $('#session_pin_' + that.getID()).click ( function(e){
            e.stopPropagation();
            if ( that.isActive() === true ) {
                mainUI.unpinSession(that._session['id']);
            } else {
                mainUI.pinSession(that._session['id']);
            }
        });
    }

    this.clean = function () {
        this._clean ();
        this._rootElement.off("click");
        this._rootElement.empty ();
        parentElement.empty ();
    }

}